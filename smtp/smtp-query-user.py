#!/usr/bin/env python

import socket
import sys

if len(sys.argv) != 3:
	print ('[-] USAGE: smtp-query-user.py <IP> <username>')
	sys.exit(0)

# Create the socket and connect
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
	print ('[+] Connecting to the server')
	connect = s.connect((sys.argv[1],25))
	banner = s.recv(1024)
	print (banner)
	print ('[+] Sending the query')
	# Test the user and print the answer
	s.send('VRFY ' + sys.argv[2] + '\r\n')
	result = s.recv(1024)
	print ('[+] Server response:')
	print (result)
	# Close the socket
	s.close()

except:
	print ('[-] Could not connect to the server')
	sys.exit(0)
